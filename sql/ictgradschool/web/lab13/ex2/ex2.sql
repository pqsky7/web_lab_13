# Answers to exercise 2 questions
SELECT
  s.fname,
  s.lname
FROM unidb_students AS s, unidb_attend AS a
WHERE s.id = a.id
      AND a.dept = "COMP"
      AND a.num = "219"
ORDER BY s.id;

SELECT
  s.fname,
  s.lname
FROM unidb_students AS s, unidb_courses AS c
WHERE s.id = c.rep_id
      AND s.country != "NZ"
ORDER BY s.id;

SELECT l.office
FROM unidb_lecturers AS l, unidb_teach AS t
WHERE l.staff_no = t.staff_no
      AND t.num = "219"
ORDER BY l.office;

SELECT DISTINCT
  s.fname,
  s.lname
FROM unidb_lecturers AS l, unidb_students AS s, unidb_teach AS t, unidb_attend AS a
WHERE l.fname = "Te Taka"
      AND l.staff_no = t.staff_no
      AND t.dept = a.dept
      AND t.num = a.num
      AND a.id = s.id;

SELECT
  s1.fname AS StdFName,
  s1.lname AS StdFName,
  s2.fname AS MentorFName,
  s2.lname AS MentorLName
FROM unidb_students AS s1, unidb_students AS s2
WHERE s2.id = s1.mentor;

SELECT
  l.fname AS LectureFName,
  l.lname AS LectureLName
FROM unidb_lecturers AS l
WHERE l.office LIKE "G%"
UNION
SELECT
  s.fname AS StdFName,
  s.lname AS StdLName
FROM unidb_students AS s
WHERE s.country != "NZ";

SELECT
  l.fname AS CORDFName,
  l.lname AS CORDLName,
  s.fname AS RepStdFName,
  s.lname AS RepStdLName
FROM unidb_courses AS c, unidb_lecturers AS l, unidb_students AS s
WHERE c.dept = "COMP" AND c.num = "219" AND c.coord_no = l.staff_no AND c.rep_id = s.id

