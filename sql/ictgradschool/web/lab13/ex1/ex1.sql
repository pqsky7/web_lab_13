# Answers to exercise 1 questions
SELECT
  dept,
  num
FROM unidb_courses;

SELECT semester
FROM unidb_attend;

SELECT
  dept,
  num
FROM unidb_attend;

SELECT
  fname,
  lname,
  country
FROM unidb_students
ORDER BY fname;

SELECT
  fname,
  lname,
  mentor
FROM unidb_students
ORDER BY mentor;

SELECT
  fname,
  lname,
  country
FROM unidb_students
ORDER BY fname;

SELECT *
FROM unidb_lecturers
ORDER BY office;

SELECT staff_no
FROM unidb_lecturers
WHERE staff_no > 500
ORDER BY staff_no;

SELECT *
FROM unidb_students
WHERE id > 1663 AND id < 1824
ORDER BY id;

SELECT *
FROM unidb_students
WHERE country IN ("NZ","AU","US")
ORDER BY id;

SELECT *
FROM unidb_lecturers
WHERE office LIKE "G%"
ORDER BY office;

SELECT *
FROM unidb_courses
  WHERE dept!="COMP"
ORDER BY dept;

SELECT *
FROM unidb_students
WHERE country IN ("FR","MX")
ORDER BY id;