DROP TABLE IF EXISTS db1ab13_ex07_player;
DROP TABLE IF EXISTS db1ab13_ex07_team;
DROP TABLE IF EXISTS db1ab13_ex07_league;

CREATE TABLE db1ab13_ex07_league (
  leagueName VARCHAR(99) NOT NULL,
  PRIMARY KEY (leagueName)
);

CREATE TABLE db1ab13_ex07_team (
  teamName     VARCHAR(50) NOT NULL,
  city         VARCHAR(99) NOT NULL,
  point        INT(99)     NOT NULL,
  playerNumber INT(25)     NOT NULL,
  teamID       VARCHAR(99) NOT NULL,
  leagueName   VARCHAR(99) NOT NULL,
  PRIMARY KEY (teamName),
  FOREIGN KEY (leagueName) REFERENCES db1ab13_ex07_league (leagueName)
);

CREATE TABLE db1ab13_ex07_player (
  id           INT(50)      NOT NULL AUTO_INCREMENT,
  fname        VARCHAR(99)  NOT NULL,
  lname        VARCHAR(99)  NOT NULL,
  age          INT(99)      NOT NULL,
  Nationality  CHAR(2)      NOT NULL,
  leagueName   VARCHAR(99)  NOT NULL,
  teamName     VARCHAR(99)  NOT NULL,
  playerID VARCHAR(999) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (leagueName) REFERENCES db1ab13_ex07_league (leagueName),
  FOREIGN KEY (teamName) REFERENCES db1ab13_ex07_team (teamName)
);

INSERT INTO db1ab13_ex07_league (leagueName) VALUES
  ('Auckland University ICT League'),
  ('Bristol University League');

INSERT INTO db1ab13_ex07_team (teamName, city, point, playerNumber, teamID, leagueName) VALUES
  ('Heroes', 'Auckland', 0, 2, 'T01', 'Auckland University ICT League');

INSERT INTO db1ab13_ex07_player (fname, lname, age, Nationality, leagueName, teamName, playerID) VALUES
  ('James', 'Lam', 24, 'CN', 'Auckland University ICT League', 'Heroes', 1),
  ('Jongwoo', 'Won', 28, 'KR', 'Auckland University ICT League','Heroes', 2);


